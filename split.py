import math
import os
import random
import sys

cmu_dict_aligned = open("cmudict-0.7b-aligned", "r", encoding="ISO-8859-1")

line_count = 0
for line in cmu_dict_aligned:
	line_count += 1
#END for
print("line count: " + str(line_count))
cmu_dict_aligned.seek(0,0)

# Distribute entire data in 2 files, alteranting between the 2
file_1 = open("cmudict-0.7b-aligned-file-1", "w+", encoding="ISO-8859-1")
file_2 = open("cmudict-0.7b-aligned-file-2", "w+", encoding="ISO-8859-1")

which_file = 1
for line in cmu_dict_aligned:
	if which_file == 1:
		file_1.write(line)
	else:
		file_2.write(line)
	#END if
	which_file = 3 - which_file
#END for
file_1.seek(0,0)
file_2.seek(0,0)

file_1_line_count = (line_count + 1) // 2
file_2_line_count = line_count - file_1_line_count
print("file 1 line count: " + str(file_1_line_count))
print("file 2 line count: " + str(file_2_line_count))

training_percentage = float(sys.argv[1])
training_lines = math.ceil(line_count * training_percentage / 100)
test_lines = line_count - training_lines
print("training lines: " + str(training_lines))
print("test lines: " + str(test_lines))

training_file = open("cmudict-0.7b-training", "w", encoding="ISO-8859-1")
labelled_test_file = open("cmudict-0.7b-labelled-test", "w+", encoding="ISO-8859-1")

if training_percentage >= 50:
	#file_1 is entirely used for training, rest of the training data comes from file_2

	for line_1 in file_1:
		training_file.write(line_1)
	#END for

	training_lines_remaining = training_lines - file_1_line_count
	test_lines_remaining = test_lines
	print("training lines remaining: " + str(training_lines_remaining))
	print("test lines remaining: " + str(test_lines_remaining))
	remaining_training_percentage = (training_percentage - 50) * 2
	print("remaining training percentage: " + str(remaining_training_percentage))
	
	for line_2 in file_2:
		rand_int = random.randint(1,100)
		if rand_int <= remaining_training_percentage:
			training_file.write(line_2)
			training_lines_remaining -= 1
		else:
			labelled_test_file.write(line_2)
			test_lines_remaining -= 1
		#END if

		if training_lines_remaining == 0 or test_lines_remaining == 0:
			break
		#END if
	#END for

	if training_lines_remaining == 0:
		for line_2 in file_2:
			labelled_test_file.write(line_2)
		#END for
	#END if

	if test_lines_remaining == 0:
		for line_2 in file_2:
			training_file.write(line_2)
		#END for
	#END if	

else:
	#file_2 is entirely used for testing, rest of the test_data comes from file_1

	for line_2 in file_2:
		labelled_test_file.write(line_2)
	#END for

	training_lines_remaining = training_lines
	test_lines_remaining = test_lines - file_2_line_count
	print("training lines remaining: " + str(training_lines_remaining))
	print("test lines remaining: " + str(test_lines_remaining))
	remaining_training_percentage = training_percentage * 2
	print("remaining training percentage: " + str(remaining_training_percentage))

	for line_1 in file_1:
		rand_int = random.randint(1,100)
		if rand_int <= remaining_training_percentage:
			training_file.write(line_1)
			training_lines_remaining -= 1
		else:
			labelled_test_file.write(line_1)
			test_lines_remaining -= 1
		#END if

		if training_lines_remaining == 0 or test_lines_remaining == 0:
			break
		#END if
	#END for

	if training_lines_remaining == 0:
		for line_1 in file_1:
			labelled_test_file.write(line_1)
		#END for
	#END if

	if test_lines_remaining == 0:
		for line_1 in file_1:
			training_file.write(line_1)
		#END for
	#END if
#END if

test_file = open("cmudict-0.7b-test", "w", encoding="ISO-8859-1")
label_file = open("cmudict-0.7b-label", "w", encoding="ISO-8859-1")

labelled_test_file.seek(0,0)
for line in labelled_test_file:
	# Split the test data and the known labels into separate files
	letters_sounds = line.strip().split("\t")
	grapheme = letters_sounds[0]
	phoneme = letters_sounds[1]
	test_file.write(grapheme + "\n")
	label_file.write(phoneme + "\n")
#END for

training_file.close()
labelled_test_file.close()
file_1.close()
file_2.close()
cmu_dict_aligned.close()
test_file.close()
label_file.close()

os.system("rm cmudict-0.7b-aligned-file-1")
os.system("rm cmudict-0.7b-aligned-file-2")
os.system("rm cmudict-0.7b-labelled-test")