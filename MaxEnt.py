training_file = open("cmudict-0.7b-training", "r", encoding="ISO-8859-1")

letter_dict = {}
chunk_count = 0
for line in training_file:
	grapheme_and_phoneme = line.split("\t")
	grapheme = grapheme_and_phoneme[0]
	phoneme = grapheme_and_phoneme[1]
	letters = grapheme.split("|")
	sounds = phoneme.split("|")
	#print("grapheme: " + grapheme)
	#print("phoneme: " + phoneme)
	#print("length of letters: " + str(len(letters)))
	#print()

	for i in range(len(letters) - 1):
		letter = letters[i]
		sound = sounds[i]
		chunk = letter + "<->" + sound
		l2s_dict = {}
		#print("letter: " + letter)
		#print("sound: " + sound)

		if letter in letter_dict:
			l2s_dict = letter_dict[letter]
			if chunk in l2s_dict:
				l2s_dict[chunk] += 1
			else:
				l2s_dict[chunk] = 1
			#END if
		else:
			l2s_dict[chunk] = 1
			letter_dict[letter] = l2s_dict
		#END if

		chunk_count += 1
		print("chunk count: " + str(chunk_count))
#END for
letter_count = len(letter_dict)
print("letter count: " + str(letter_count))

training_file.close()

#print("letter_dict")
#print(letter_dict)
print("length of letter_dict: " + str(len(letter_dict)))