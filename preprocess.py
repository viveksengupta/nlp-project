import os
import re

new_word_regex = re.compile("[A-Za-z]*\s.*")

os.system("tail -n +127 cmudict-0.7b > cmudict-0.7b-temp")

cmu_dict_temp = open("cmudict-0.7b-temp","r", encoding="ISO-8859-1")
cmu_dict_formatted = open("cmudict-0.7b-formatted","w")

#x = 0;

for line in cmu_dict_temp:
	#if x == 10:
		#break;
	#END if
	word_and_phones = line.split()
	#print("word_and_phones: " + str(word_and_phones))
	word_and_phones_length = len(word_and_phones)
	#print("length of word_and_phones: " + str(word_and_phones_length))
	word = word_and_phones[0]
	#print("word: " + word)
	if new_word_regex.match(line) != None:
		#print("not a repeated word or a word with numbers or apostrophes")
		new_word = ""
		word_length = len(word)
		#print("length of word: " + str(word_length))
		for i in range(word_length-1):
			new_word += word[i] + " "
		#END for
		new_word += word[word_length-1]
		#print("new word: " + new_word)
		word_and_phones[0] = new_word
		#print("word_and_phones: " + str(word_and_phones))
		new_line = ""
		new_line += word_and_phones[0] + "\t"
		for j in range(1,word_and_phones_length-1,1):
			new_line += word_and_phones[j] + " "
		#END for
		new_line += word_and_phones[word_and_phones_length-1]
		print(new_line)
		cmu_dict_formatted.write(new_line + "\n")
	#END if
	
	#x += 1
#END for

cmu_dict_temp.close()
cmu_dict_formatted.close()

os.system("rm cmudict-0.7b-temp")